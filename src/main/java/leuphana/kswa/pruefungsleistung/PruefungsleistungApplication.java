package leuphana.kswa.pruefungsleistung;

import de.leuphana.swa.documentsystem.structure.Document;
import de.leuphana.swa.messagingsystem.behaviour.Sendable;
import de.leuphana.swa.printingsystem.behaviour.Printable;
import de.leuphana.swa.printingsystem.structure.PrintResult;
import de.leuphana.swa.statisticssystem.behaviour.Countable;
import org.apache.camel.*;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PruefungsleistungApplication {

    static CamelContext camelContext = new DefaultCamelContext();

    public static void main(String[] args) throws Exception {
        SpringApplication.run(PruefungsleistungApplication.class, args);

        camelContext.addRoutes(new RouteBuilder() {

            @Override
            public void configure() throws Exception {
                from("direct:start")
                        .to("class:de.leuphana.swa.documentsystem.behaviour.DocumentSystem?method=createDocument")
                        .process(new Processor() {
                            @Override
                            public void process(Exchange exchange) throws Exception {
                                Document document = exchange.getIn().getBody(Document.class);
                                Printable printable = new Printable(document.getDocumentName(), document.getDocumentBody().getContent());
                                exchange.getOut().setBody(printable);
                            }
                        })
                        .to("seda:document")
                        .to("class:de.leuphana.swa.printingsystem.behaviour.PrintingSystem?method=print")
                        .process(new Processor() {
                            @Override
                            public void process(Exchange exchange) throws Exception {
                                PrintResult printResult = exchange.getIn().getBody(PrintResult.class);
                                Sendable sendable = new Sendable(printResult.getContent(), printResult.getNumberOfPages(), printResult.getPricePerPage());
                                exchange.getOut().setBody((sendable));
                            }
                        })
                        .to("seda:end")
                        .to("class:de.leuphana.swa.messagingsystem.behaviour.MessagingSystem?method=sendMessage")
                        .process(new Processor() {
                            @Override
                            public void process(Exchange exchange) throws Exception {

                                Sendable sendable = exchange.getIn().getBody(Sendable.class);
                                Countable countable = new Countable(sendable.numberOfPages, sendable.getPricePerPage());

                                exchange.getOut().setBody(countable);
                            }
                        }).to("seda:message")

                        .to("seda:statistic")
                        .to("class:de.leuphana.swa.statisticssystem.behaviour.StatisticsSystem?method=addEntry");
            }
        });

        camelContext.start();

        ProducerTemplate producerTemplate = camelContext.createProducerTemplate();
        producerTemplate.sendBody("direct:start", "TestDocument");
        
//        ConsumerTemplate consumerTemplate = camelContext.createConsumerTemplate();
//        Printable printable = consumerTemplate.receiveBody("seda:document", Printable.class);
//        System.out.println(printable.getContent());

//        producerTemplate.sendBody("class:de.leuphana.swa.printingsystem.behaviour.PrintingSystem?method=print", printable);
//        Sendable sendable = consumerTemplate.receiveBody("seda:end", Sendable.class);
////        System.out.println("Content : " + sendable.getContent() + "Number of Pages: " + sendable.getNumberOfPages() + "Price per Page: " + sendable.getPricePerPage());
//
//        producerTemplate.sendBody("class:de.leuphana.swa.messagingsystem.behaviour.MessagingSystem?method=sendMessage", sendable);
//        Countable countable = consumerTemplate.receiveBody("seda:message", Countable.class);
////        System.out.println(countable.numberOfPages + countable.pricePerPage);
//
//        producerTemplate.sendBody("class:de.leuphana.swa.statisticssystem.behaviour.StatisticsSystem?method=addEntry", countable);

    }
}